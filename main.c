#include <stdio.h>
#include <time.h>

int istSchaltjahr1(int);
int istSchaltjahr2(int);

int main() {
    time_t startzeit, endzeit;
    int schaltjahre = 0, jahre = 1000000000;

    time(&startzeit);
    for (int i = 1; i <= jahre; i++) {
        if(istSchaltjahr1(i)) {
            schaltjahre++;
        }
    }
    time(&endzeit);

    printf("Variante 1 \n");
    printf("Laufzeit: %lf \n", difftime(endzeit, startzeit));
    printf("Schaltjahre: %i \n", schaltjahre);

    schaltjahre = 0;

    time(&startzeit);
    for (int i = 1; i <= jahre; i++) {
        if(istSchaltjahr2(i)) {
            schaltjahre++;
        }
    }
    time(&endzeit);

    printf("Variante 2 \n");
    printf("Laufzeit: %lf \n", difftime(endzeit, startzeit));
    printf("Schaltjahre: %i \n", schaltjahre);

    return 0;
}

int istSchaltjahr1(int n) {
    if(n % 4 == 0 && !(n % 100 == 0 && n % 400 != 0)) {
        return 1;
    } else {
        return 0;
    }
}

int istSchaltjahr2(int n) {
    if((n % 400 == 0 || n % 100 != 0) && n % 4 == 0) {
        return 1;
    } else {
        return 0;
    }
}